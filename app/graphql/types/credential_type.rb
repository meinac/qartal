# frozen_string_literal: true

module Types
  class CredentialType < Types::BaseObject
    authorize! :read

    field :id, ID, null: false
    field :organization, Types::OrganizationType, null: false
    field :created_by, Types::UserType, null: false
    field :updated_by, Types::UserType, null: false
    field :updated_by, Types::UserType, null: false
    field :type, String, null: false
    field :name, String, null: false
    field :description, String, null: true
    field :payload, GraphQL::Types::JSON, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
