# frozen_string_literal: true

module Types
  class OrganizationType < Types::BaseObject
    authorize! :read

    field :id, ID, null: false
    field :parent_organization, Types::OrganizationType, null: true
    field :sub_organizations, Types::OrganizationType.connection_type, null: false
    field :memberships, Types::MembershipType.connection_type, null: false do
      argument :role, String, required: false
    end
    field :credentials, Types::CredentialType.connection_type, null: false do
      argument :type, String, required: false
    end
    field :name, String, null: false
    field :description, String, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false

    def credentials(type:)
      object.credentials.by_type(type)
    end

    def memberships(role:)
      return object.memberships.where(role: role) if role

      object.memberships
    end
  end
end
