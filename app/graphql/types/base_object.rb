# frozen_string_literal: true

module Types
  class BaseObject < GraphQL::Schema::Object
    edge_type_class(Types::BaseEdge)
    connection_type_class(Types::BaseConnection)
    field_class Types::BaseField

    class << self
      def authorize!(policy)
        policies << policy
      end

      def authorized?(object, context)
        super && apply_type_policies(context[:current_user], object)
      rescue Pundit::NotAuthorizedError
        false
      end

      private

      def apply_type_policies(user, object)
        policies.each { |policy| Pundit.authorize(user, object, :"#{policy}?") }
      end

      def policies
        @policies ||= []
      end
    end
  end
end
