# frozen_string_literal: true

module Types
  class MemberRoleType < Types::BaseEnum
    value :admin, "Members with admin role can manage organizations"
    value :member, "Members with member role can only view organization data"
  end
end
