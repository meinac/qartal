# frozen_string_literal: true

module Types
  class ErrorType < Types::BaseObject
    field :key, String, null: false
    field :messages, [String], null: false

    def key
      object.first
    end

    def messages
      object.second.map { |obj| obj[:error] }
    end
  end
end
