# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    field :me, Types::UserType, null: true, description: "Returns the current user"

    field :credential, Types::CredentialType, null: true, description: "Returns the credential by given ID" do
      argument :id, ID, required: true
    end

    field :credential_types, [String], null: false, description: "Returns the supported credential types"
    field :credential_schema, GraphQL::Types::JSON, null: true, description: "Returns the payload schema for the given credential type" do
      argument :type, String, required: true
    end

    field :membership, Types::MembershipType, null: true, description: "Returns the member by given ID" do
      argument :id, ID, required: true
    end

    field :organization, Types::OrganizationType, null: true, description: "Returns the organization by given ID" do
      argument :id, ID, required: true
    end

    def me
      raise GraphQL::ExecutionError, "You are not authenticated!" unless context[:current_user]

      context[:current_user]
    end

    def organization(id:)
      Organization.find(id)
    end

    def credential(id:)
      Credential.find(id)
    end

    def membership(id:)
      Membership.find(id)
    end

    def credential_types
      Credential.supported_types
    end

    def credential_schema(type:)
      file_path = Credential.schema_file_path_for(type)

      File.read(file_path).then { |file| JSON.parse(file) } if file_path
    end
  end
end
