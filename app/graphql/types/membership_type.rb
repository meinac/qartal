# frozen_string_literal: true

module Types
  class MembershipType < Types::BaseObject
    authorize! :read

    field :id, ID, null: false
    field :organization, Types::OrganizationType, null: false
    field :user, Types::UserType, null: false
    field :role, Types::MemberRoleType, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
