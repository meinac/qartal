# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    field :organization_create, mutation: Mutations::OrganizationCreate
    field :credential_create, mutation: Mutations::CredentialCreate
    field :credential_delete, mutation: Mutations::CredentialDelete
    field :credential_update, mutation: Mutations::CredentialUpdate
    field :credential_test, mutation: Mutations::CredentialTest
    field :membership_create, mutation: Mutations::MembershipCreate
    field :membership_delete, mutation: Mutations::MembershipDelete
    field :membership_update, mutation: Mutations::MembershipUpdate
    field :signin, mutation: Mutations::Signin
    field :signup, mutation: Mutations::Signup
  end
end
