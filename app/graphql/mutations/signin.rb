# frozen_string_literal: true

module Mutations
  class Signin < BaseMutation
    anonymous_mutation!

    argument :email, String, required: true
    argument :password, String, required: true

    field :user, Types::UserType, null: true
    field :errors, [Types::ErrorType], null: true
    field :access_token, String, null: true

    def resolve(email:, password:)
      user = User.find_by_email(email)

      if user&.authenticate(password)
        { user: user, access_token: user.access_token }
      else
        { errors: basic_error(:authentication, :failed) }
      end
    end
  end
end
