# frozen_string_literal: true

module Mutations
  class MembershipUpdate < BaseMutation
    argument :id, ID, required: true
    argument :role, String, required: true

    field :membership, Types::MembershipType, null: false
    field :errors, [Types::ErrorType], null: true

    def resolve(params)
      service_response = Memberships::UpdateService.execute(user: current_user, **params)

      if service_response.success?
        { membership: service_response.result }
      else
        { errors: service_response.errors }
      end
    end
  end
end
