# frozen_string_literal: true

module Mutations
  class MembershipCreate < BaseMutation
    argument :organization_id, ID, required: true
    argument :email, String, required: true
    argument :role, String, required: true

    field :membership, Types::MembershipType, null: true
    field :errors, [Types::ErrorType], null: true

    def resolve(params)
      service_response = Memberships::CreateService.execute(user: current_user, **params)

      if service_response.success?
        { membership: service_response.result }
      else
        { errors: service_response.errors }
      end
    end
  end
end
