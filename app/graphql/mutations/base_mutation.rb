# frozen_string_literal: true

module Mutations
  class BaseMutation < GraphQL::Schema::RelayClassicMutation
    argument_class Types::BaseArgument
    field_class Types::BaseField
    input_object_class Types::BaseInputObject
    object_class Types::BaseObject

    # Wraps the given error information into an array
    # to match ActiveModel#Errors.details method response
    def basic_error(key, value)
      [[key, [{ error: value }]]]
    end

    def current_user
      context[:current_user]
    end

    def ready?(*)
      raise AuthenticationError unless self.class.anonymous_mutation? || current_user

      true
    end

    class << self
      def anonymous_mutation!
        @anonymous_mutation = true
      end

      def anonymous_mutation?
        @anonymous_mutation
      end
    end
  end
end
