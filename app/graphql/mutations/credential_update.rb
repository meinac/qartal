# frozen_string_literal: true

module Mutations
  class CredentialUpdate < BaseMutation
    argument :id, ID, required: true
    argument :name, String, required: true
    argument :description, String, required: false
    argument :payload, GraphQL::Types::JSON, required: true

    field :credential, Types::CredentialType, null: true
    field :errors, [Types::ErrorType], null: true

    def resolve(id:, **params)
      service_response = Credentials::UpdateService.execute(id: id, user: current_user, params: params)

      if service_response.success?
        { credential: service_response.result }
      else
        { errors: service_response.errors }
      end
    end
  end
end
