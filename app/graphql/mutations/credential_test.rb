# frozen_string_literal: true

module Mutations
  class CredentialTest < BaseMutation
    argument :type, String, required: true
    argument :payload, GraphQL::Types::JSON, required: true

    field :connected, Boolean, null: false

    def resolve(*)
      sleep(2)

      { connected: true }
    end
  end
end
