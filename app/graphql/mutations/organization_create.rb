# frozen_string_literal: true

module Mutations
  class OrganizationCreate < BaseMutation
    argument :name, String, required: true
    argument :description, String, required: false
    argument :parent_id, ID, required: false

    field :organization, Types::OrganizationType, null: true
    field :errors, [Types::ErrorType], null: true

    def resolve(params)
      service_response = Organizations::CreateService.execute(user: current_user, **params)

      if service_response.success?
        { organization: service_response.result }
      else
        { errors: service_response.errors }
      end
    end
  end
end
