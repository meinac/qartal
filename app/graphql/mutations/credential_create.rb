# frozen_string_literal: true

module Mutations
  class CredentialCreate < BaseMutation
    argument :organization_id, ID, required: true
    argument :type, String, required: true
    argument :name, String, required: true
    argument :description, String, required: false
    argument :payload, GraphQL::Types::JSON, required: true

    field :credential, Types::CredentialType, null: true
    field :errors, [Types::ErrorType], null: true

    def resolve(organization_id:, **params)
      service_response = Credentials::CreateService.execute(user: current_user, organization_id: organization_id, params: params)

      if service_response.success?
        { credential: service_response.result }
      else
        { errors: service_response.errors }
      end
    end
  end
end
