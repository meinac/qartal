# frozen_string_literal: true

module Mutations
  class Signup < BaseMutation
    anonymous_mutation!

    argument :first_name, String, required: true
    argument :last_name, String, required: true
    argument :email, String, required: true
    argument :password, String, required: true

    field :user, Types::UserType, null: true
    field :errors, [Types::ErrorType], null: true
    field :access_token, String, null: true

    def resolve(params)
      user = User.create(params)

      if user.persisted?
        { user: user, access_token: user.access_token }
      else
        { errors: user.errors.details.to_a }
      end
    end
  end
end
