# frozen_string_literal: true

module Mutations
  class CredentialDelete < BaseMutation
    argument :id, ID, required: true

    field :deleted, Boolean, null: false
    field :errors, [Types::ErrorType], null: true

    def resolve(params)
      service_response = Credentials::DeleteService.execute(user: current_user, **params)

      if service_response.success?
        { deleted: true }
      else
        { errors: service_response.errors, deleted: false }
      end
    end
  end
end
