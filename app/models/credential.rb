# frozen_string_literal: true

class Credential < ApplicationRecord
  belongs_to :organization
  belongs_to :created_by, class_name: "User"
  belongs_to :updated_by, class_name: "User"

  validates :type, presence: true, inclusion: { in: -> (_) { supported_types } }
  validates :name, presence: true, uniqueness: { scope: :organization_id }, length: { maximum: 128 }
  validates :description, length: { maximum: 512 }
  validates :payload, presence: true
  validate :payload_schema

  before_save :encrypt_payload

  scope :by_type, -> (type) { type ? where(type: type) : all }

  class << self
    # I am planning to move this to somewhere else at some point
    # to make it easier to install 3rd party types.
    def supported_types
      @supported_types ||= Dir.children(schemas_path).map { |file| file.sub(".json", "") }
    end

    def schemas_path
      @schemas_path ||= Rails.root.join("app", "models", "credentials", "schemas")
    end

    def schema_file_path_for(type)
      return unless type.in?(supported_types)

      schemas_path.join("#{type}.json")
    end

    # Disable the STI.
    def inheritance_column
      :_disabled
    end
  end

  def payload=(value)
    @payload = value&.stringify_keys
    @payload_changed = true
  end

  def payload
    @payload ||= ready_to_decipher? ? decrypted_payload : {}
  end

  private

  def ready_to_decipher?
    cipher_text.present? && iv.present?
  end

  def decrypted_payload
    Encryption::Decryptor.execute(cipher_text, iv).then { |json| JSON.parse(json) }
  end

  def encrypt_payload
    return true unless @payload_changed

    @payload_changed = false
    self.cipher_text, self.iv = Encryption::Encryptor.execute(payload.to_json)
  end

  def payload_schema
    return unless type.in?(self.class.supported_types) && !schema.valid?(payload)

    errors.add(:payload, :invalid)
  end

  def schema
    @schema ||= JSONSchemer.schema(Pathname.new(schema_file_path))
  end

  def schema_file_path
    self.class.schema_file_path_for(type)
  end
end
