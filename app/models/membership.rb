# frozen_string_literal: true

class Membership < ApplicationRecord
  belongs_to :organization
  belongs_to :user

  enum role: { member: 0, admin: 1 }

  scope :by_user, -> (user) { where(user: user) }

  validates :user_id, uniqueness: { scope: :organization_id, case_sensitive: false }
  validates :role, presence: true
end
