# frozen_string_literal: true

class Organization < ApplicationRecord
  WITH_ANCESTORS_SQL_TEMPLATE = <<~SQL
    WITH RECURSIVE organizations_with_ancestors AS (
      %<base_query>s
      UNION
      SELECT
        organizations.*
      FROM
        organizations_with_ancestors
      INNER JOIN organizations ON organizations.id = organizations_with_ancestors.parent_id
    )
    SELECT
      *
    FROM
      organizations_with_ancestors
    WHERE
      organizations_with_ancestors.parent_id IS NULL
  SQL

  belongs_to :parent_organization, class_name: "Organization", foreign_key: :parent_id, required: false

  has_many :sub_organizations, class_name: "Organization", foreign_key: :parent_id
  has_many :memberships
  has_many :users, through: :memberships
  has_many :credentials

  validates :name, presence: true, length: { maximum: 255 }, uniqueness: { scope: :parent_id }
  validates :description, length: { maximum: 512 }

  def self.top_level_for(user)
    format(WITH_ANCESTORS_SQL_TEMPLATE,
           base_query: user.organizations.to_sql).then { |sql| find_by_sql(sql) }
  end

  # TODO: We can preload all the parent records of the organization
  # or, we can calculate the result with a single recursive query.
  def admin?(user)
    memberships.admin.by_user(user).exists? || admin_of_parent?(user)
  end

  def member?(user)
    memberships.by_user(user).exists? || member_of_parent?(user)
  end

  private

  def admin_of_parent?(user)
    parent_organization&.admin?(user)
  end

  def member_of_parent?(user)
    parent_organization&.member?(user)
  end
end
