# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  def deleted_in_db?
    !self.class.find_by_id(id)
  end
end
