# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  has_many :memberships
  has_many :organizations, through: :memberships

  validates :first_name, presence: true, length: { maximum: 255 }
  validates :last_name, presence: true, length: { maximum: 255 }
  validates :email, presence: true, uniqueness: { case_sensitive: false }, format: /\A[^@\s]+@[^@\s]+\z/

  def access_token
    JsonWebToken.encode(id)
  end

  def top_level_organizations
    Organization.top_level_for(self)
  end
end
