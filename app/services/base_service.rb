# frozen_string_literal: true

# This is an abstract class that whould be inherited by all the service
# classes. Classes inheriting this abstract class must override the
# `execute` instance method.
#
# This abstraction also provides a DSL to set accessors for the arguments
# provided by the client of the service object, like so;
#
#   class MyServiceClass < BaseService
#     uses :organization_id
#
#     def execute
#       puts organization_id # => 1
#     end
#   end
#
#   MyServiceClass.execute(organization_id: 1)
#
# To see a real example of this abstraction, you can check the
# `Organizations::CreateService` service class
#
class BaseService
  include MethodCallbacks
  include Annotations::Transactional

  class ServiceResponse
    attr_reader :status, :result, :errors

    def initialize(status, result: nil, errors: nil)
      @status = status
      @result = result
      @errors = errors
    end

    def success?
      status == :success
    end

    def failure?
      !success?
    end
  end

  class AuthorizationError < RuntimeError
    def initialize(message = "Not Authorized!")
      super
    end
  end

  class << self
    def uses(*arg_names)
      arg_names.each do |arg_name|
        define_method(arg_name) { args.fetch(arg_name, nil) }
      end
    end

    def execute(**args)
      new(**args).execute
    end

    def authorize!(policy, on:)
      authorizations << [policy, on]

      before_send(:execute, :authorize!)
    end

    def authorizations
      @authorizations ||= []
    end
  end

  def initialize(**args)
    @args = args
  end

  def execute
    raise "Not Implemented"
  end

  private

  attr_reader :args

  def authorize!
    self.class.authorizations.each do |policy, on|
      subject = send(on)

      subject ? Pundit.authorize(user, subject, :"#{policy}?") : not_authorized!
    rescue Pundit::NotAuthorizedError
      not_authorized!
    end
  end

  def success(result)
    ServiceResponse.new(:success, result: result)
  end

  def failure(errors = [])
    ServiceResponse.new(:failure, errors: errors)
  end

  def not_authorized!
    raise AuthorizationError
  end

  def respond_with(object)
    object.errors.any? ? failure(object.errors.details.to_a) : success(object)
  end
end
