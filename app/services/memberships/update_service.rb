# frozen_string_literal: true

module Memberships
  class UpdateService < BaseService
    uses :user, :id, :role

    authorize! :update, on: :membership

    def execute
      membership.update(role: role)

      respond_with(membership)
    end

    private

    def membership
      @membership ||= Membership.find_by_id(id)
    end
  end
end
