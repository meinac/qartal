# frozen_string_literal: true

module Memberships
  class CreateService < BaseService
    uses :user, :organization_id, :email, :role

    authorize! :manage_members, on: :organization

    def execute
      membership = Membership.create(organization: organization, user: member_user, role: role)

      respond_with(membership)
    end

    private

    def organization
      @organization ||= Organization.find_by_id(organization_id)
    end

    def member_user
      @member_user ||= User.find_by_email(email)
    end
  end
end
