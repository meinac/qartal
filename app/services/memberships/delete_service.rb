# frozen_string_literal: true

module Memberships
  class DeleteService < BaseService
    uses :user, :id

    authorize! :delete, on: :membership

    def execute
      membership.destroy

      respond_with(membership)
    end

    private

    def membership
      @membership ||= Membership.find_by_id(id)
    end
  end
end
