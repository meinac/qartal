# frozen_string_literal: true

module Organizations
  class CreateService < BaseService
    uses :user, :parent_id, :name, :description

    transactional :execute
    def execute
      not_authorized! unless authorized?
      organization.memberships.admin.create!(organization: organization, user: user) if organization.persisted?

      respond_with(organization)
    end

    private

    def authorized?
      return true unless parent_organization

      parent_organization.admin?(user)
    end

    def parent_organization
      @parent_organization ||= Organization.find(parent_id) if parent_id
    end

    def organization
      @organization ||= Organization.create(parent_id: parent_id, name: name, description: description)
    end
  end
end
