# frozen_string_literal: true

module Credentials
  class UpdateService < BaseService
    uses :user, :id, :params

    authorize! :update, on: :credential

    def execute
      credential.update(update_params)

      respond_with(credential)
    end

    private

    def credential
      @credential ||= Credential.find_by_id(id)
    end

    def update_params
      params.merge(updated_by: user)
    end
  end
end
