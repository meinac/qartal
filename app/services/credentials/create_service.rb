# frozen_string_literal: true

module Credentials
  class CreateService < BaseService
    uses :user, :organization_id, :params

    authorize! :manage_credentials, on: :organization

    def execute
      credential = Credential.create(create_params)

      respond_with(credential)
    end

    private

    def organization
      @organization ||= Organization.find_by_id(organization_id)
    end

    def create_params
      params.to_h.merge(
        organization: organization,
        created_by: user,
        updated_by: user
      )
    end
  end
end
