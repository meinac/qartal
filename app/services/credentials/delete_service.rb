# frozen_string_literal: true

module Credentials
  class DeleteService < BaseService
    uses :user, :id

    authorize! :delete, on: :credential

    def execute
      credential.destroy

      respond_with(credential)
    end

    private

    def credential
      @credential ||= Credential.find_by_id(id)
    end
  end
end
