# frozen_string_literal: true

class GraphqlController < ApplicationController
  def execute
    variables = prepare_variables(params[:variables])
    query = params[:query]
    operation_name = params[:operationName]
    result = QartalSchema.execute(query, variables: variables, context: context, operation_name: operation_name)
    render json: result
  rescue AuthenticationError
    handle_authentication_error
  rescue StandardError => e
    raise e unless Rails.env.development?

    handle_error_in_development(e)
  end

  private

  # Handle variables in form data, JSON body, or a blank value
  def prepare_variables(variables_param) # rubocop:disable Metrics/MethodLength
    case variables_param
    when String
      if variables_param.present?
        JSON.parse(variables_param) || {}
      else
        {}
      end
    when Hash
      variables_param
    when ActionController::Parameters
      variables_param.to_unsafe_hash # GraphQL-Ruby will validate name and type of incoming variables.
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{variables_param}"
    end
  end

  def handle_authentication_error
    render json: { errors: [{ message: "Authentication Error" }], data: {} }, status: 401
  end

  def handle_error_in_development(err)
    logger.error err.message
    logger.error err.backtrace.join("\n")

    render json: { errors: [{ message: err.message, backtrace: err.backtrace }], data: {} }, status: 500
  end

  def context
    { current_user: current_user }
  end

  def current_user
    User.find_by_id(current_user_id) if current_user_id
  end

  def current_user_id
    @current_user_id ||= JsonWebToken.decode(access_token) if access_token
  end

  def access_token
    @access_token ||= begin
      token = request.headers["HTTP_AUTHORIZATION"]

      token&.match(/\ABearer (.+)/i)&.captures&.first
    end
  end
end
