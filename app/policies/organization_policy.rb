# frozen_string_literal: true

class OrganizationPolicy < ApplicationPolicy
  def read?
    record.member?(user)
  end

  def manage_members?
    record.admin?(user)
  end

  def manage_credentials?
    record.admin?(user)
  end
end
