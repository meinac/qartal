# frozen_string_literal: true

class CredentialPolicy < ApplicationPolicy
  def read?
    OrganizationPolicy.new(user, record.organization).read?
  end

  def update?
    OrganizationPolicy.new(user, record.organization).manage_credentials?
  end

  alias_method :delete?, :update?
end
