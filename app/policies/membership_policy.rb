# frozen_string_literal: true

class MembershipPolicy < ApplicationPolicy
  def read?
    OrganizationPolicy.new(user, record.organization).read?
  end

  def update?
    OrganizationPolicy.new(user, record.organization).manage_members?
  end

  def delete?
    update? && user != record.user
  end
end
