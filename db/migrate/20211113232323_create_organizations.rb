# frozen_string_literal: true

class CreateOrganizations < ActiveRecord::Migration[7.0]
  def change
    create_table :organizations, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.references :parent, type: :uuid, index: true, foreign_key: { to_table: :organizations, on_delete: :cascade }
      t.string :name, limit: 255, null: false

      t.index :name, unique: true, where: 'parent_id IS NULL'
      t.index [:name, :parent_id], unique: true, where: 'parent_id IS NOT NULL'
      t.timestamps
    end
  end
end
