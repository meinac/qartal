class CreateCredentials < ActiveRecord::Migration[7.0]
  def change
    create_table :credentials, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.references :organization, type: :uuid, null: false, foreign_key: { on_delete: :cascade }
      t.references :created_by, type: :uuid, null: false, foreign_key: { on_delete: :cascade, to_table: :users }
      t.references :updated_by, type: :uuid, null: false, foreign_key: { on_delete: :cascade, to_table: :users }
      t.string :name, limit: 128, null: false
      t.string :description, limit: 512
      t.string :type, null: false
      t.string :cipher_text, null: false
      t.string :iv, null: false

      t.timestamps
    end
  end
end
