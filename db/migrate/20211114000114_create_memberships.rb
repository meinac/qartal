# frozen_string_literal: true

class CreateMemberships < ActiveRecord::Migration[7.0]
  def change
    create_table :memberships, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.references :organization, type: :uuid, null: false, index: false, foreign_key: { on_delete: :cascade }
      t.references :user, type: :uuid, null: false, index: false, foreign_key: { on_delete: :cascade }
      t.integer :role, default: 0, null: false

      t.timestamps

      t.index [:organization_id, :user_id], unique: true
      t.index [:user_id, :organization_id]
    end
  end
end
