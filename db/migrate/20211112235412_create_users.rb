# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users, id: :uuid, default: -> { "uuid_generate_v4()" } do |t|
      t.string :first_name, limit: 255, null: false
      t.string :last_name, limit: 255, null: false
      t.citext :email, null: false, index: { unique: true }
      t.string :password_digest, null: false
      t.string :reset_password_token, index: { unique: true }

      t.timestamps
    end
  end
end
