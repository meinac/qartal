class AddDescriptionToOrganizations < ActiveRecord::Migration[7.0]
  def change
    change_table :organizations do |t|
      t.string :description, limit: 512
    end
  end
end
