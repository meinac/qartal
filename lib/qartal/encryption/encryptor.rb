# frozen_string_literal: true

module Encryption
  class Encryptor < Base
    def self.execute(payload)
      new(payload).execute
    end

    def initialize(payload)
      @payload = payload
    end

    def execute
      [encoded_cipher_text, encoded_init_vector]
    end

    private

    attr_reader :payload, :init_vector

    def encoded_cipher_text
      Base64.encode64(cipher_text)
    end

    def encoded_init_vector
      Base64.encode64(init_vector)
    end

    def cipher_text
      cipher.update(payload) + cipher.final
    end

    def cipher
      @cipher ||= super.tap do |c|
        c.encrypt
        @init_vector = c.iv = c.random_iv
        c.key = key
      end
    end
  end
end
