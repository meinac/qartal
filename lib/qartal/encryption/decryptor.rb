# frozen_string_literal: true

module Encryption
  class Decryptor < Base
    def self.execute(cipher_text, init_vector)
      new(cipher_text, init_vector).execute
    end

    def initialize(cipher_text, init_vector)
      @cipher_text = cipher_text
      @init_vector = init_vector
    end

    def execute
      cipher.update(decoded_cipher_text) + cipher.final
    end

    private

    attr_reader :cipher_text, :init_vector

    def cipher
      @cipher ||= super.tap do |c|
        c.decrypt
        c.iv = decoded_init_vector
        c.key = key
      end
    end

    def decoded_cipher_text
      Base64.decode64(cipher_text)
    end

    def decoded_init_vector
      Base64.decode64(init_vector)
    end
  end
end
