# frozen-string-literal: true

require "openssl"

module Encryption
  class Base
    def self.key
      Base64.decode64(EasyConf.app_config.ENCRYPTION_KEY)
    end

    def cipher
      OpenSSL::Cipher.new("aes-256-cbc")
    end

    delegate :key, to: :"self.class", private: true
  end
end
