# frozen_string_literal: true

module Annotations
  module Transactional
    def self.included(base)
      base.include(MethodCallbacks)
      base.extend(DSL)
    end

    def run_within_transaction(&block)
      ActiveRecord::Base.transaction { block.call }
    end

    module DSL
      def transactional(method_name)
        around_send(method_name, :run_within_transaction)
      end
    end
  end
end
