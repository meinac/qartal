# frozen_string_literal: true

class JsonWebToken
  TOKEN_EXPIRES_IN = 24.hours
  ALGORITHM = "HS256"

  class << self
    def encode(user_id)
      payload = { id: user_id, exp: (Time.zone.now + TOKEN_EXPIRES_IN).to_i }

      JWT.encode(payload, encryption_key, ALGORITHM)
    end

    def decode(token)
      JWT.decode(token, encryption_key, true, algorithm: ALGORITHM).first.fetch("id")
    rescue StandardError # rubocop:disable Lint/SuppressedException
    end

    private

    def encryption_key
      EasyConf.app_config.ENCRYPTION_KEY
    end
  end
end
