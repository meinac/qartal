# frozen_string_literal: true

module MethodCallbacks
  def self.included(base)
    base.extend(DSL)
  end

  private

  def _run_before_send_callback_for(method_name)
    callback = self.class._before_send_callbacks[method_name]

    send(callback)
  end

  def _run_around_send_callback_for(method_name, &block)
    callback = self.class._around_send_callbacks[method_name]

    send(callback, &block)
  end

  module DSL
    def before_send(method_name, callback)
      _before_send_callbacks[method_name] = callback
    end

    def around_send(method_name, callback)
      _around_send_callbacks[method_name] = callback
    end

    def method_added(method_name)
      super

      return if _has_registered_callback?(method_name)
      return _register_before_callback(method_name) if _before_send_callbacks.key?(method_name)
      return _register_around_callback(method_name) if _around_send_callbacks.key?(method_name)
    end

    def _before_send_callbacks
      @_before_send_callbacks ||= {}
    end

    def _around_send_callbacks
      @_around_send_callbacks ||= {}
    end

    private

    def _has_registered_callback?(method_name)
      _registered_callbacks.include?(method_name)
    end

    def _registered_callbacks
      @_registered_callbacks ||= []
    end

    def _register_before_callback(method_name)
      new_method_name = _prepare_for_callback(method_name)

      define_method(method_name) do |*args|
        _run_before_send_callback_for(method_name)

        send(new_method_name, *args)
      end
    end

    def _register_around_callback(method_name)
      new_method_name = _prepare_for_callback(method_name)

      define_method(method_name) do |*args|
        _run_around_send_callback_for(method_name) do
          send(new_method_name, *args)
        end
      end
    end

    def _prepare_for_callback(method_name)
      _registered_callbacks << method_name

      "original_#{method_name}".tap do |new_method_name|
        alias_method(new_method_name, method_name)
      end
    end
  end
end
