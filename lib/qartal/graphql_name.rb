# frozen_string_literal: true

module GraphqlName
  refine String do
    def graphql_name
      camelize(:lower)
    end
  end

  refine Symbol do
    def graphql_name
      to_s.graphql_name
    end
  end
end
