# frozen_string_literal: true

RSpec.describe Memberships::UpdateService do
  describe "#execute" do
    let(:user) { create(:user) }
    let(:membership) { create(:membership, :member) }
    let(:role) { "admin" }

    let(:service_object) { described_class.new(user: user, id: membership.id, role: role) }

    subject(:update_membership) { service_object.execute }

    context "authorization" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "is expected to authorize the user" do
        update_membership

        expect(Pundit).to have_received(:authorize).with(user, membership, :update?)
      end
    end

    context "when the user does not have permission to update the membership" do
      before do
        allow(Pundit).to receive(:authorize).with(user, membership, :update?).and_raise(Pundit::NotAuthorizedError)
      end

      it "is expected to raise an authorization error" do
        expect { update_membership }.to raise_error(BaseService::AuthorizationError)
      end
    end

    context "when the user has permission to update the membership" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      context "when updating the membership fails" do
        let(:role) { nil }

        it "returns the failure result along with the error messages" do
          expect(update_membership).to be_failure
          expect(update_membership.errors).to eq([[:role, [{ error: :blank }]]])
        end
      end

      context "when updating the membership succeeds" do
        it "changes the role of the membership" do
          expect { update_membership }.to change { membership.reload.role }.from("member").to("admin")
        end

        it "returns the success result" do
          expect(update_membership).to be_success
          expect(update_membership.result).to be_an_instance_of(Membership)
        end
      end
    end
  end
end
