# frozen_string_literal: true

RSpec.describe Memberships::CreateService do
  describe "#execute" do
    let(:user) { create(:user) }
    let(:new_member) { create(:user) }
    let(:organization) { create(:organization) }
    let(:role) { "member" }

    let(:service_object) { described_class.new(user: user, organization_id: organization.id, email: new_member.email, role: role) }

    subject(:create_membership) { service_object.execute }

    context "authorization" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "is expected to authorize the user" do
        create_membership

        expect(Pundit).to have_received(:authorize).with(user, organization, :manage_members?)
      end
    end

    context "when the user does not have permission to add members" do
      before do
        allow(Pundit).to receive(:authorize).with(user, organization, :manage_members?).and_raise(Pundit::NotAuthorizedError)
      end

      it "is expected to raise an authorization error" do
        expect { create_membership }.to raise_error(BaseService::AuthorizationError)
      end
    end

    context "when the user has permission to add members" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      context "when the membership creation fails" do
        let(:role) { nil }

        it "does not create the membership" do
          expect { create_membership }.not_to change { organization.memberships.count }
        end

        it "returns the failure result along with the error messages" do
          expect(create_membership).to be_failure
          expect(create_membership.errors).to eq([[:role, [{ error: :blank }]]])
        end
      end

      context "when the membership creation succeeds" do
        it "creates a new membership record for the organization" do
          expect { create_membership }.to change { organization.memberships.count }.by(1)
        end

        it "returns the success result" do
          expect(create_membership).to be_success
          expect(create_membership.result).to be_an_instance_of(Membership)
        end
      end
    end
  end
end
