# frozen_string_literal: true

RSpec.describe Memberships::DeleteService do
  describe "#execute" do
    let(:user) { create(:user) }
    let(:membership) { create(:membership, :member) }

    let(:service_object) { described_class.new(user: user, id: membership.id) }

    subject(:delete_membership) { service_object.execute }

    context "authorization" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "is expected to authorize the user" do
        delete_membership

        expect(Pundit).to have_received(:authorize).with(user, membership, :delete?)
      end
    end

    context "when the user does not have permission to delete the membership" do
      before do
        allow(Pundit).to receive(:authorize).with(user, membership, :delete?).and_raise(Pundit::NotAuthorizedError)
      end

      it "is expected to raise an authorization error" do
        expect { delete_membership }.to raise_error(BaseService::AuthorizationError)
      end
    end

    context "when the user has permission to delete the membership" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "removes membership" do
        expect { delete_membership }.to change { membership.deleted_in_db? }.to(true)
      end

      it "returns the success result" do
        expect(delete_membership).to be_success
        expect(delete_membership.result).to be_an_instance_of(Membership)
      end
    end
  end
end
