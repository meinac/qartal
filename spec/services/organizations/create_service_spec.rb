# frozen_string_literal: true

RSpec.describe Organizations::CreateService do
  describe "#execute" do
    let(:user) { create(:user) }
    let(:name) { "Artifical Organization" }
    let(:description) { "Foo" }
    let(:service_object) { described_class.new(user: user, parent_id: parent_id, name: name, description: description) }

    subject(:create_organization) { service_object.execute }

    context "when the `parent_id` is given" do
      let(:parent_organization) { create(:organization) }
      let(:parent_id) { parent_organization.id }

      before do
        create(:membership, user: user, organization: parent_organization, role: role)
      end

      context "when the user is not an admin of the parent organization" do
        let(:role) { :member }

        it "is expected to raise an authorization error" do
          expect { create_organization }.to raise_error(BaseService::AuthorizationError)
        end
      end

      context "when the user is an admin of the parent organization" do
        let(:role) { :admin }

        context "when the organization creation fails" do
          let(:name) { nil }

          it "does not create the organization" do
            expect { create_organization }.not_to change { parent_organization.sub_organizations.count }
          end

          it "returns the failure result along with the error messages" do
            expect(create_organization).to be_failure
            expect(create_organization.errors).to eq([[:name, [{ error: :blank }]]])
          end
        end

        context "when the organization creation succeeds" do
          it "creates the organization and makes the member admin" do
            expect { create_organization }.to change { parent_organization.sub_organizations.count }.by(1)
                                          .and change { user.memberships.admin.count }.by(1)
          end

          it "returns the success result" do
            expect(create_organization).to be_success
            expect(create_organization.result).to be_an_instance_of(Organization)
          end
        end
      end
    end

    context "when the `parent_id` is not given" do
      let(:parent_id) { nil }

      context "when the organization creation fails" do
        let(:name) { nil }

        it "does not create the organization" do
          expect { create_organization }.not_to change { Organization.count }
        end

        it "returns the failure result along with the error messages" do
          expect(create_organization).to be_failure
          expect(create_organization.errors).to eq([[:name, [{ error: :blank }]]])
        end
      end

      context "when the organization creation succeeds" do
        it "creates the organization and makes the member admin" do
          expect { create_organization }.to change { Organization.count }.by(1)
                                        .and change { user.memberships.admin.count }.by(1)
        end

        it "returns the success result" do
          expect(create_organization).to be_success
          expect(create_organization.result).to be_an_instance_of(Organization)
        end
      end
    end
  end
end
