# frozen_string_literal: true

RSpec.describe Credentials::UpdateService do
  describe "#execute" do
    let(:user) { create(:user) }
    let(:credential) { create(:credential, :postgresql, name: "Test") }
    let(:name) { "Test Credential" }
    let(:params) { { name: name } }

    let(:service_object) { described_class.new(user: user, id: credential.id, params: params) }

    subject(:update_credential) { service_object.execute }

    context "authorization" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "is expected to authorize the user" do
        update_credential

        expect(Pundit).to have_received(:authorize).with(user, credential, :update?)
      end
    end

    context "when the user does not have permission to update the credential" do
      before do
        allow(Pundit).to receive(:authorize).with(user, credential, :update?).and_raise(Pundit::NotAuthorizedError)
      end

      it "is expected to raise an authorization error" do
        expect { update_credential }.to raise_error(BaseService::AuthorizationError)
      end
    end

    context "when the user has permission to update the credential" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      context "when updating the credential fails" do
        let(:name) { nil }

        it "returns the failure result along with the error messages" do
          expect(update_credential).to be_failure
          expect(update_credential.errors).to eq([[:name, [{ error: :blank }]]])
        end
      end

      context "when updating the credential succeeds" do
        it "changes the role of the credential" do
          expect { update_credential }.to change { credential.reload.name }.to(name)
        end

        it "returns the success result" do
          expect(update_credential).to be_success
          expect(update_credential.result).to be_an_instance_of(Credential)
        end
      end
    end
  end
end
