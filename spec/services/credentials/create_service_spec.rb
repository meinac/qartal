# frozen_string_literal: true

RSpec.describe Credentials::CreateService do
  describe "#execute" do
    let(:user) { create(:user) }
    let(:organization) { create(:organization) }
    let(:name) { "Test" }
    let(:test_payload) { build(:credential, :postgresql).payload }
    let(:params) { { type: "postgresql", name: name, description: "Foo", payload: test_payload } }

    let(:service_object) { described_class.new(user: user, organization_id: organization.id, params: params) }

    subject(:create_credential) { service_object.execute }

    context "authorization" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "is expected to authorize the user" do
        create_credential

        expect(Pundit).to have_received(:authorize).with(user, organization, :manage_credentials?)
      end
    end

    context "when the user does not have permission to create credential" do
      before do
        allow(Pundit).to receive(:authorize).with(user, organization, :manage_credentials?).and_raise(Pundit::NotAuthorizedError)
      end

      it "is expected to raise an authorization error" do
        expect { create_credential }.to raise_error(BaseService::AuthorizationError)
      end
    end

    context "when the user has permission to create credential" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      context "when the credential creation fails" do
        let(:name) { nil }

        it "does not create the credential" do
          expect { create_credential }.not_to change { organization.credentials.count }
        end

        it "returns the failure result along with the error messages" do
          expect(create_credential).to be_failure
          expect(create_credential.errors).to eq([[:name, [{ error: :blank }]]])
        end
      end

      context "when the credential creation succeeds" do
        it "creates a new credential record for the organization" do
          expect { create_credential }.to change { organization.credentials.count }.by(1)
        end

        it "returns the success result" do
          expect(create_credential).to be_success
          expect(create_credential.result).to be_an_instance_of(Credential)
        end
      end
    end
  end
end
