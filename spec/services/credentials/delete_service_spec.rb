# frozen_string_literal: true

RSpec.describe Credentials::DeleteService do
  describe "#execute" do
    let(:user) { create(:user) }
    let(:credential) { create(:credential, :postgresql) }

    let(:service_object) { described_class.new(user: user, id: credential.id) }

    subject(:delete_credential) { service_object.execute }

    context "authorization" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "is expected to authorize the user" do
        delete_credential

        expect(Pundit).to have_received(:authorize).with(user, credential, :delete?)
      end
    end

    context "when the user does not have permission to delete the credential" do
      before do
        allow(Pundit).to receive(:authorize).with(user, credential, :delete?).and_raise(Pundit::NotAuthorizedError)
      end

      it "is expected to raise an authorization error" do
        expect { delete_credential }.to raise_error(BaseService::AuthorizationError)
      end
    end

    context "when the user has permission to delete the credential" do
      before do
        allow(Pundit).to receive(:authorize)
      end

      it "removes credential" do
        expect { delete_credential }.to change { credential.deleted_in_db? }.to(true)
      end

      it "returns the success result" do
        expect(delete_credential).to be_success
        expect(delete_credential.result).to be_an_instance_of(Credential)
      end
    end
  end
end
