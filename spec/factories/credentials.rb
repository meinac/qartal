# frozen_string_literal: true

FactoryBot.define do
  factory :credential do
    organization
    created_by factory: :user
    updated_by factory: :user
    cipher_text { "5ArkmWAuLoeWDcrUawQDzA==\n" }
    iv { "Vnp/Zb+Yj7P8wX7TJ+RMBg==\n" }

    name { "Foo Bar" }

    trait :postgresql do
      type { :postgresql }

      payload do
        {
          adapter: "postgresql",
          host: "localhost",
          port: 5432,
          username: "foo",
          password: "123456",
          database: "test"
        }
      end
    end
  end
end
