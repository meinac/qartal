# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    first_name { "John" }
    last_name { "Doe" }
    sequence(:email) { |i| "foo_#{i}@bar" }
    password { "123456" }
  end
end
