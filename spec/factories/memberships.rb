# frozen_string_literal: true

FactoryBot.define do
  factory :membership do
    organization
    user

    trait :member do
      role { :member }
    end
  end
end
