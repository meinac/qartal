# frozen_string_literal: true

FactoryBot.define do
  factory :organization do
    sequence :name do |i|
      "Acme #{i}"
    end
  end
end
