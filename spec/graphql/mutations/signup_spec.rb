# frozen_string_literal: true

RSpec.describe Mutations::Signup do
  describe "arguments" do
    subject { described_class }

    it { is_expected.to have_graphql_argument(:first_name).of_type(GraphQL::Types::String) }
    it { is_expected.to have_graphql_argument(:last_name).of_type(GraphQL::Types::String) }
    it { is_expected.to have_graphql_argument(:email).of_type(GraphQL::Types::String) }
    it { is_expected.to have_graphql_argument(:password).of_type(GraphQL::Types::String) }
  end

  describe "fields" do
    subject { described_class }

    it { is_expected.to have_graphql_field(:user).of_type(Types::UserType) }
    it { is_expected.to have_graphql_field(:errors).of_type(Types::ErrorType).as_list }
    it { is_expected.to have_graphql_field(:access_token).of_type(GraphQL::Types::String) }
  end

  describe "#resolve" do
    let(:params) do
      {
        first_name: "john.doe@localhost",
        last_name: "john.doe@localhost",
        email: "john.doe@localhost",
        password: "123456"
      }
    end

    before do
      mock_user.validate

      allow(User).to receive(:create).and_return(mock_user)

      execute_mutation(params: params, include_fields_of: [:errors])
    end

    context "when the creation of the user record fails" do
      let(:mock_user) { build(:user, email: "foo") }

      it "does not return any user information" do
        expect(response).to have_response_field(:user).as(nil)
      end

      it "does not return a token" do
        expect(response).to have_response_field(:access_token).as(nil)
      end

      it "returns the error information" do
        expect(response).to have_response_field(:errors).including({ "key" => "email", "messages" => ["invalid"] })
      end
    end

    context "when the creation of the user record succeeds" do
      let(:mock_user) { create(:user) }

      it "returns the user information" do
        expect(response).to have_response_field(:user).as({ "__typename" => "User" })
      end

      it "returns the access token" do
        expect(response).to have_response_field(:access_token).type(String)
      end

      it "does not return any error information" do
        expect(response).to have_response_field(:errors).as(nil)
      end
    end
  end
end
