# frozen_string_literal: true

RSpec.describe Mutations::OrganizationCreate do
  describe "arguments" do
    subject { described_class }

    it { is_expected.to have_graphql_argument(:name).of_type(GraphQL::Types::String) }
    it { is_expected.to have_graphql_argument(:description).of_type(GraphQL::Types::String).optional }
    it { is_expected.to have_graphql_argument(:parent_id).of_type(GraphQL::Types::ID).optional }
  end

  describe "fields" do
    subject { described_class }

    it { is_expected.to have_graphql_field(:organization).of_type(Types::OrganizationType) }
    it { is_expected.to have_graphql_field(:errors).of_type(Types::ErrorType).as_list }
  end

  describe "#resolve" do
    let(:user) { create(:user) }
    let(:membership) { create(:membership, :member, user: user) }
    let(:organization) { membership.organization }
    let(:request_params) { { name: "Acme Org", parent_id: nil, description: "foo" } }
    let(:mock_service_response) { instance_double(BaseService::ServiceResponse, success?: true, result: organization) }

    before do
      allow(Organizations::CreateService).to receive(:execute).and_return(mock_service_response)

      execute_mutation(user: user, params: request_params, include_fields_of: [:errors])
    end

    it "calls the `Organizations::CreateService` with provided arguments and `current_user`" do
      expect(Organizations::CreateService).to have_received(:execute).with(user: user, **request_params)
    end

    context "when organization creation fails" do
      let(:mock_service_response) { instance_double(BaseService::ServiceResponse, success?: false, errors: [["error_key", [{ error: "message" }]]]) }

      it "does not return any organization information" do
        expect(response).to have_response_field(:organization).as(nil)
      end

      it "returns the error information" do
        expect(response).to have_response_field(:errors).including({ "key" => "error_key", "messages" => ["message"] })
      end
    end

    context "when organization creation succeeds" do
      it "returns the organization information" do
        expect(response).to have_response_field(:organization).as({ "__typename" => "Organization" })
      end

      it "does not return any error information" do
        expect(response).to have_response_field(:errors).as(nil)
      end
    end
  end
end
