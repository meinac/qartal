# frozen_string_literal: true

RSpec.describe Mutations::Signin do
  describe "arguments" do
    subject { described_class }

    it { is_expected.to have_graphql_argument(:email).of_type(GraphQL::Types::String) }
    it { is_expected.to have_graphql_argument(:password).of_type(GraphQL::Types::String) }
  end

  describe "fields" do
    subject { described_class }

    it { is_expected.to have_graphql_field(:user).of_type(Types::UserType) }
    it { is_expected.to have_graphql_field(:errors).of_type(Types::ErrorType).as_list }
    it { is_expected.to have_graphql_field(:access_token).of_type(GraphQL::Types::String) }
  end

  describe "#resolve" do
    let(:email) { "john.doe@localhost" }
    let(:password) { "123456" }

    context "when the given credentials are incorrect" do
      before do
        execute_mutation(params: { email: email, password: password }, include_fields_of: [:errors])
      end

      it "does not return any user information" do
        expect(response).to have_response_field(:user).as(nil)
      end

      it "does not return a token" do
        expect(response).to have_response_field(:access_token).as(nil)
      end

      it "returns the error information" do
        expect(response).to have_response_field(:errors).including({ "key" => "authentication", "messages" => ["failed"] })
      end
    end

    context "when the given credentials are correct" do
      before do
        create(:user, email: email, password: password)

        execute_mutation(params: { email: email, password: password })
      end

      it "returns the user information" do
        expect(response).to have_response_field(:user).as({ "__typename" => "User" })
      end

      it "returns the access token" do
        expect(response).to have_response_field(:access_token).type(String)
      end

      it "does not return any error information" do
        expect(response).to have_response_field(:errors).as(nil)
      end
    end
  end
end
