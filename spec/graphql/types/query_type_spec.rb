# frozen_string_literal: true

RSpec.describe Types::QueryType do
  subject { described_class }

  it { is_expected.to have_graphql_field(:me).of_type(Types::UserType) }
  it { is_expected.to have_graphql_field(:credential_types).of_type(GraphQL::Types::String).as_list.required }

  describe "credential" do
    it { is_expected.to have_graphql_field(:credential).of_type(Types::CredentialType) }
    it { is_expected.to have_graphql_argument(:id).of_type(GraphQL::Types::ID) }
  end

  describe "credential_schema" do
    it { is_expected.to have_graphql_field(:credential_schema).of_type(GraphQL::Types::JSON) }
    it { is_expected.to have_graphql_argument(:type).of_type(GraphQL::Types::String) }
  end

  describe "membership" do
    it { is_expected.to have_graphql_field(:membership).of_type(Types::MembershipType) }
    it { is_expected.to have_graphql_argument(:id).of_type(GraphQL::Types::ID) }
  end

  describe "organization" do
    it { is_expected.to have_graphql_field(:organization).of_type(Types::OrganizationType) }
    it { is_expected.to have_graphql_argument(:id).of_type(GraphQL::Types::ID) }
  end
end
