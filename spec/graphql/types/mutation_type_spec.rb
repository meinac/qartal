# frozen_string_literal: true

RSpec.describe Types::MutationType do
  subject { described_class }

  it { is_expected.to have_graphql_field(:signin) }
  it { is_expected.to have_graphql_field(:signup) }
  it { is_expected.to have_graphql_field(:organization_create) }
  it { is_expected.to have_graphql_field(:credential_create) }
  it { is_expected.to have_graphql_field(:credential_delete) }
  it { is_expected.to have_graphql_field(:credential_update) }
  it { is_expected.to have_graphql_field(:credential_test) }
  it { is_expected.to have_graphql_field(:membership_create) }
  it { is_expected.to have_graphql_field(:membership_delete) }
  it { is_expected.to have_graphql_field(:membership_update) }
end
