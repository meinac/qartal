# frozen_string_literal: true

RSpec.describe Types::UserType do
  subject { described_class }

  it { is_expected.to have_graphql_field(:id).of_type(GraphQL::Types::ID).required }
  it { is_expected.to have_graphql_field(:first_name).of_type(GraphQL::Types::String).required }
  it { is_expected.to have_graphql_field(:last_name).of_type(GraphQL::Types::String).required }
  it { is_expected.to have_graphql_field(:email).of_type(GraphQL::Types::String).required }
  it { is_expected.to have_graphql_field(:top_level_organizations).of_type(Types::OrganizationType).as_collection.required }
  it { is_expected.to have_graphql_field(:memberships).of_type(Types::MembershipType).as_collection.required }
  it { is_expected.to have_graphql_field(:created_at).of_type(GraphQL::Types::ISO8601DateTime).required }
  it { is_expected.to have_graphql_field(:updated_at).of_type(GraphQL::Types::ISO8601DateTime).required }
end
