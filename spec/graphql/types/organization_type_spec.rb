# frozen_string_literal: true

RSpec.describe Types::OrganizationType do
  subject { described_class }

  it { is_expected.to have_graphql_field(:id).of_type(GraphQL::Types::ID).required }
  it { is_expected.to have_graphql_field(:parent_organization).of_type(Types::OrganizationType) }
  it { is_expected.to have_graphql_field(:sub_organizations).of_type(Types::OrganizationType).as_collection.required }
  it { is_expected.to have_graphql_field(:name).of_type(GraphQL::Types::String).required }
  it { is_expected.to have_graphql_field(:description).of_type(GraphQL::Types::String) }
  it { is_expected.to have_graphql_field(:created_at).of_type(GraphQL::Types::ISO8601DateTime).required }
  it { is_expected.to have_graphql_field(:updated_at).of_type(GraphQL::Types::ISO8601DateTime).required }

  describe "memberships" do
    it { is_expected.to have_graphql_field(:memberships).of_type(Types::MembershipType).as_collection.required }
    it { is_expected.to have_graphql_argument(:role).of_type(GraphQL::Types::String).optional }
  end

  describe "credentials" do
    it { is_expected.to have_graphql_field(:credentials).of_type(Types::CredentialType).as_collection.required }
    it { is_expected.to have_graphql_argument(:type).of_type(GraphQL::Types::String).optional }
  end
end
