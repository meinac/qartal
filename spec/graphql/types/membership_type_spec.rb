# frozen_string_literal: true

RSpec.describe Types::MembershipType do
  subject { described_class }

  it { is_expected.to have_graphql_field(:id).of_type(GraphQL::Types::ID).required }
  it { is_expected.to have_graphql_field(:organization).of_type(Types::OrganizationType).required }
  it { is_expected.to have_graphql_field(:user).of_type(Types::UserType).required }
  it { is_expected.to have_graphql_field(:role).of_type(Types::MemberRoleType).required }
  it { is_expected.to have_graphql_field(:created_at).of_type(GraphQL::Types::ISO8601DateTime).required }
  it { is_expected.to have_graphql_field(:updated_at).of_type(GraphQL::Types::ISO8601DateTime).required }
end
