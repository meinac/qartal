# frozen_string_literal: true

RSpec.describe Types::CredentialType do
  subject { described_class }

  it { is_expected.to have_graphql_field(:id).of_type(GraphQL::Types::ID).required }
  it { is_expected.to have_graphql_field(:type).of_type(GraphQL::Types::String).required }
  it { is_expected.to have_graphql_field(:name).of_type(GraphQL::Types::String).required }
  it { is_expected.to have_graphql_field(:description).of_type(GraphQL::Types::String) }
  it { is_expected.to have_graphql_field(:payload).of_type(GraphQL::Types::JSON).required }
  it { is_expected.to have_graphql_field(:organization).of_type(Types::OrganizationType).required }
  it { is_expected.to have_graphql_field(:created_by).of_type(Types::UserType).required }
  it { is_expected.to have_graphql_field(:updated_by).of_type(Types::UserType).required }
  it { is_expected.to have_graphql_field(:created_at).of_type(GraphQL::Types::ISO8601DateTime).required }
  it { is_expected.to have_graphql_field(:updated_at).of_type(GraphQL::Types::ISO8601DateTime).required }
end
