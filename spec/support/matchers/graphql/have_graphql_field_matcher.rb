# frozen_string_literal: true

HAVE_GRAPHQL_FIELD_FAILURE_MESSAGES = {
  no_match: "%{klass} does not have a field named `%{field_name}`",
  required: "Expected `%{field_name}` to be an optional field but it was required",
  optional: "Expected `%{field_name}` to be a required field but it was optional",
  is_a_list: "Expected `%{field_name}` not to be a list but it was a list",
  not_a_list: "Expected `%{field_name}` to be a list but it wasn't",
  wrong_type: "Expected `%{field_name}` to be `%{expected_type}` but it was `%{actual_type}`"
}.freeze

RSpec::Matchers.define :have_graphql_field do |field_name|
  using GraphqlName

  chain(:of_type) { |value| @of_type = value }
  chain(:as_collection) { @as_collection = true }
  chain(:as_list) { @as_list = true }
  chain(:required) { @required = true }

  def fail_with(reason)
    @failure_reason = HAVE_GRAPHQL_FIELD_FAILURE_MESSAGES[reason]

    false
  end

  match do |klass|
    @field = klass.fields[field_name.graphql_name]
    check_type = @as_collection ? @of_type.connection_type : @of_type

    return fail_with(:no_match) unless @field
    return fail_with(:required) if !@required && @field.type.non_null?
    return fail_with(:optional) if @required && !@field.type.non_null?
    return fail_with(:is_a_list) if !@as_list && @field.type.list?
    return fail_with(:not_a_list) if @as_list && !@field.type.list?
    return fail_with(:wrong_type) if @of_type && check_type != @field.type.unwrap

    true
  end

  failure_message do |klass|
    format_args = { klass: klass, field_name: field_name, expected_type: @of_type, actual_type: @field&.type&.unwrap.inspect }

    format(@failure_reason, format_args)
  end
end
