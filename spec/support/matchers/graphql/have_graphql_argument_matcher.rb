# frozen_string_literal: true

RSpec::Matchers.define :have_graphql_argument do |arg_name|
  using GraphqlName

  chain(:of_type) { |value| @of_type = value }
  chain(:optional) { @optional = true }

  def field_class(klass)
    if klass < Mutations::BaseMutation
      klass
    else
      field_name = RSpec.current_example.metadata[:example_group][:description_args].first

      klass.fields[field_name.graphql_name]
    end
  end

  match do |klass|
    argument = field_class(klass).arguments[arg_name.graphql_name]

    return false unless argument
    return false if @optional && argument.type.non_null?
    return false if !@optional && !argument.type.non_null?
    return false if @of_type && @of_type != argument.type.unwrap

    true
  end
end
