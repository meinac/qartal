# frozen_string_literal: true

RSpec::Matchers.define :have_response_field do |field_name|
  chain(:as) do |value|
    @as = value
    @as_set = true

    raise "Can not use both `as` and `including` chains" if @including_set
  end

  chain(:including) do |value|
    @including = value
    @including_set = true

    raise "Can not use both `as` and `including` chains" if @as_set
  end

  chain(:type) do |value|
    @type = value
  end

  match do
    return false unless has_response_field?(field_name)
    return false if @as_set && graphql_response(field_name) != @as
    return false if @including_set && !graphql_response(field_name).include?(@including)
    return false if @type && !graphql_response(field_name).is_a?(@type)

    true
  end

  failure_message do
    return "Expected the GraphQL response to have `#{field_name}` field but it didn't" unless has_response_field?(field_name)
    return "Expected the `#{field_name}` to be `#{@as.inspect}` but it was `#{graphql_response(field_name).inspect}`" if @as_set
    return "Expected the `#{field_name}` to include `#{@including.inspect}` but it didn't. The actual value was `#{graphql_response(field_name).inspect}`" if @including_set

    "Expected the `#{field_name}` to be an instance of `#{@type.inspect}` but it was `#{graphql_response(field_name).class.inspect}`"
  end
end
