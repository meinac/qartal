# frozen_string_literal: true

module ConfigHelper
  def mock_config(config_key, config_value)
    allow(config_double).to receive(config_key).and_return(config_value)
  end

  private

  def config_double
    @config_double ||= double(:config).tap { |config| allow(EasyConf).to receive(:app_config).and_return(config) }
  end
end
