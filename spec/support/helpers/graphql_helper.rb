# frozen_string_literal: true

module GraphqlHelper
  using GraphqlName

  def execute_mutation(params:, include_fields_of: [], user: nil)
    query_builder = MutationQueryBuilder.new(field_name_of_described_class, described_class, params, include_fields_of)

    headers = {}
    headers.merge!("Authorization" => "Bearer #{user.access_token}") if user

    post "/", params: { query: query_builder.query }, headers: headers
  end

  def has_response_field?(field_name)
    top_level_field_data.key?(field_name.graphql_name)
  end

  def graphql_response(field_name)
    top_level_field_data[field_name.graphql_name]
  end

  private

  def top_level_field_data
    response.parsed_body.dig("data", field_name_of_described_class)
  end

  def field_name_of_described_class
    definition = Types::MutationType.fields.find { |_, field| field.mutation == described_class }
    raise "Mutation field is not found!" unless definition

    definition.first
  end

  class MutationQueryBuilder
    def initialize(name, klass, params, include_fields_of)
      @name = name
      @klass = klass
      @params = params
      @include_fields_of = include_fields_of.map(&:graphql_name)
    end

    def query
      <<~QUERY
        mutation {
          #{name} (input: {#{graphql_arguments}})
          {
            #{select_all_fields_of(klass)}
          }
        }
      QUERY
    end

    private

    attr_reader :name, :klass, :params, :include_fields_of

    def graphql_arguments
      params.transform_keys(&:graphql_name)
            .map { |key, value| "#{key}: #{value.to_json}" }
            .join("\n")
    end

    def select_all_fields_of(klass)
      klass.fields.map { |field, type| select_field(field, type.type.unwrap) }.join("\n")
    end

    def select_field(field, type)
      return field if type.kind.scalar?

      fields = include_fields_of.include?(field) ? select_all_fields_of(type) : "__typename"

      "#{field} { #{fields} }"
    end
  end
end
