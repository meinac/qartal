# frozen_string_literal: true

RSpec.describe Organization do
  describe "associations" do
    it { is_expected.to belong_to(:parent_organization).required(false) }
    it { is_expected.to have_many(:sub_organizations) }
    it { is_expected.to have_many(:memberships) }
    it { is_expected.to have_many(:users).through(:memberships) }
    it { is_expected.to have_many(:credentials) }
  end

  describe "validations" do
    subject { create(:organization) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(255) }
    it { is_expected.to validate_length_of(:description).is_at_most(512) }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:parent_id) }
  end

  describe ".top_level_for" do
    let!(:top_level_organization_1) { create(:organization) }
    let!(:top_level_organization_2) { create(:organization) }
    let!(:level_1_organization_1) { create(:organization, parent_organization: top_level_organization_1) }
    let!(:level_1_organization_2) { create(:organization) }
    let!(:level_2_organization) { create(:organization, parent_organization: level_1_organization_1) }
    let!(:user) { create(:user) }

    before do
      create(:membership, user: user, organization: level_2_organization)
    end

    subject { described_class.top_level_for(user) }

    it { is_expected.to eq([top_level_organization_1]) }
  end

  describe "#admin?" do
    let(:organization) { create(:organization) }
    let(:user) { create(:user) }

    subject { organization.admin?(user) }

    context "when the user is admin of the organization" do
      before do
        create(:membership, role: :admin, organization: organization, user: user)
      end

      it { is_expected.to be_truthy }
    end

    context "when the user is not admin of the organization" do
      context "when the user is admin of a parent organization" do
        let(:parent_organization) { create(:organization) }

        before do
          organization.update!(parent_organization: parent_organization)

          create(:membership, role: :admin, organization: parent_organization, user: user)
        end

        it { is_expected.to be_truthy }
      end

      context "when the user is not admin of a parent organization" do
        it { is_expected.to be_falsey }
      end
    end
  end

  describe "#member?" do
    let(:organization) { create(:organization) }
    let(:user) { create(:user) }

    subject { organization.member?(user) }

    context "when the user is admin of the organization" do
      before do
        create(:membership, role: :admin, organization: organization, user: user)
      end

      it { is_expected.to be_truthy }
    end

    context "when the user is not admin of the organization" do
      context "when the user is member of the organization" do
        before do
          create(:membership, role: :member, organization: organization, user: user)
        end

        it { is_expected.to be_truthy }
      end

      context "when the user is not member of the organization" do
        context "when the user is admin of a parent organization" do
          let(:parent_organization) { create(:organization) }

          before do
            organization.update!(parent_organization: parent_organization)

            create(:membership, role: :admin, organization: parent_organization, user: user)
          end

          it { is_expected.to be_truthy }
        end

        context "when the user is not admin of a parent organization" do
          context "when the user is member of a parent organization" do
            let(:parent_organization) { create(:organization) }

            before do
              organization.update!(parent_organization: parent_organization)

              create(:membership, role: :member, organization: parent_organization, user: user)
            end

            it { is_expected.to be_truthy }
          end

          context "when the user is not member of a parent organization" do
            it { is_expected.to be_falsey }
          end
        end
      end
    end
  end
end
