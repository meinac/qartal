# frozen_string_literal: true

RSpec.describe User do
  describe "associations" do
    it { is_expected.to have_many(:memberships) }
    it { is_expected.to have_many(:organizations).through(:memberships) }
  end

  describe "validations" do
    subject { create(:user) }

    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_length_of(:first_name).is_at_most(255) }
    it { is_expected.to validate_length_of(:last_name).is_at_most(255) }
    it { is_expected.to validate_uniqueness_of(:email).ignoring_case_sensitivity }
    it { is_expected.to allow_value("email@addresse.foo").for(:email) }
    it { is_expected.to_not allow_value("foo").for(:email) }
  end

  describe "#access_token" do
    let(:user) { create(:user) }
    let(:expected_token) { "foo" }

    subject(:access_token) { user.access_token }

    before do
      allow(JsonWebToken).to receive(:encode).and_return(expected_token)
    end

    it "calls the `JsonWebToken` library with the ID of the user" do
      expect(access_token).to eq(expected_token)
      expect(JsonWebToken).to have_received(:encode).with(user.id)
    end
  end

  describe "#top_level_organizations" do
    let(:user) { build(:user) }

    subject(:top_level_organizations) { user.top_level_organizations }

    before do
      allow(Organization).to receive(:top_level_for).and_return([])
    end

    it "delegates the call to the Organization model" do
      expect(top_level_organizations).to eq([])
      expect(Organization).to have_received(:top_level_for).with(user)
    end
  end
end
