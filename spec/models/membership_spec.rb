# frozen_string_literal: true

RSpec.describe Membership do
  describe "associatinos" do
    it { is_expected.to belong_to(:organization) }
    it { is_expected.to belong_to(:user) }
  end

  describe "validations" do
    subject { create(:membership) }

    it { is_expected.to validate_presence_of(:role) }
    it { is_expected.to validate_uniqueness_of(:user_id).scoped_to(:organization_id).case_insensitive }
  end
end
