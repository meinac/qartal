# frozen_string_literal: true

RSpec.describe Credential do
  let(:credential) { create(:credential, :postgresql) }

  before do
    mock_config(:ENCRYPTION_KEY, "QJrwQCqnnjyD/GBRXQmE2U2sr3XEG3Hay3An0Dq/SyI=\n")
  end

  describe "associations" do
    it { is_expected.to belong_to(:organization) }
    it { is_expected.to belong_to(:created_by) }
    it { is_expected.to belong_to(:updated_by) }
  end

  describe "validations" do
    subject { credential }

    it { is_expected.to validate_presence_of(:type) }
    it { is_expected.to validate_inclusion_of(:type).in_array(["postgresql"]) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:organization_id) }
    it { is_expected.to validate_length_of(:name).is_at_most(128) }
    it { is_expected.to validate_length_of(:description).is_at_most(512) }
  end

  describe "#save" do
    # This subject is with `validate: false` because subclasses can have their
    # own validations which would prevent running the `before_save` callback.
    subject { credential.save(validate: false) }

    context "when the payload does not change" do
      it "does not change cipher_text and iv" do
        expect {
          subject
        }.not_to change { [credential.cipher_text, credential.iv] }
      end
    end

    context "when the payload changes" do
      let(:expected_cipher_text) { "foo" }
      let(:expected_iv) { "bar" }
      let(:mock_encryption_result) { [expected_cipher_text, expected_iv] }

      before do
        credential.payload = {}

        allow(Encryption::Encryptor).to receive(:execute).and_return(mock_encryption_result)
      end

      it "changes the `cipher_text` and `iv`" do
        expect {
          subject
        }.to change { credential.cipher_text }.to(expected_cipher_text)
        .and change { credential.iv }.to(expected_iv)
      end
    end
  end

  describe "#payload" do
    subject { credential.payload }

    before do
      credential.payload = preset_payload
    end

    context "when the preset payload is not nil" do
      let(:preset_payload) { { foo: :bar } }

      it { is_expected.to eq(preset_payload.stringify_keys) }
    end

    context "when the preset payload is nil" do
      let(:preset_payload) { nil }

      context "when the `cipher_text` is nil" do
        before do
          credential.cipher_text = nil
        end

        it { is_expected.to eql({}) }
      end

      context "when the `cipher_text` is not nil" do
        context "when the `iv` is nil" do
          before do
            credential.iv = nil
          end

          it { is_expected.to eql({}) }
        end

        context "when the `iv` is not nil" do
          let(:expected_payload) { { "foo" => "bar" } }

          before do
            allow(Encryption::Decryptor).to receive(:execute).and_return(expected_payload.to_json)
          end

          it { is_expected.to eql(expected_payload) }
        end
      end
    end
  end
end
