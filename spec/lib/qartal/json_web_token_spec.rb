# frozen_string_literal: true

RSpec.describe JsonWebToken do
  let(:user_id) { "4fa5540c-6eef-4f80-9977-4f94e7501143" }
  let(:token) do
    "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6IjRmYTU1NDBjLTZlZWYtNGY4MC05OTc3LTRmOTRlNzUwMTE0MyIsImV4cCI6MTY4MjI5NDQwMH0." \
      "Es6P8FRhBnLbZJ_OgW-Sub0u6hPwXwBwgBtuoNY8mj4"
  end

  before do
    mock_config(:ENCRYPTION_KEY, "QJrwQCqnnjyD/GBRXQmE2U2sr3XEG3Hay3An0Dq/SyI=\n")

    travel_to("23/04/2023")
  end

  describe ".encode" do
    subject { described_class.encode(user_id) }

    it { is_expected.to eq(token) }
  end

  describe ".decode" do
    subject { described_class.decode(token) }

    context "when the given token is valid" do
      context "when the token is not expired" do
        it { is_expected.to eq(user_id) }
      end

      context "when the given token is expired" do
        let(:token) do
          "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6IjRmYTU1NDBjLTZlZWYtNGY4MC05OTc3LTRmOTRlNzUwMTE0MyIsImV4cCI6MTYzNjg1NDkyMX0." \
            "0U3MfBMODRhNcxx7v9hTBpCZQsb7qbL35PTdb67mu_c"
        end

        it { is_expected.to be_nil }
      end
    end

    context "when the given token is not valid" do
      let(:token) { "invalid value" }

      it { is_expected.to be_nil }
    end
  end
end
