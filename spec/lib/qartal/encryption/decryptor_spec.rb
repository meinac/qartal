# frozen_string_literal: true

RSpec.describe Encryption::Decryptor do
  describe "::execute" do
    let(:cipher_text) { "21zv2WbRXJSKepS5/RbYNQ==\n" }
    let(:iv) { "Vnp/Zb+Yj7P8wX7TJ+RMBg==\n" }
    let(:expected_result) { "Foo" }
    let(:test_key) { "QJrwQCqnnjyD/GBRXQmE2U2sr3XEG3Hay3An0Dq/SyI=\n" }

    subject { described_class.execute(cipher_text, iv) }

    before do
      mock_config(:ENCRYPTION_KEY, test_key)
    end

    it { is_expected.to eql(expected_result) }
  end
end
