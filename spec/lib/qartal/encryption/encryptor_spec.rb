# frozen_string_literal: true

RSpec.describe Encryption::Encryptor do
  describe "::execute" do
    let(:payload) { "Foo" }
    let(:expected_cipher_text) { "21zv2WbRXJSKepS5/RbYNQ==\n" }
    let(:encoded_initialization_vector) { "Vnp/Zb+Yj7P8wX7TJ+RMBg==\n" }
    let(:expected_result) { [expected_cipher_text, encoded_initialization_vector] }
    let(:test_key) { "QJrwQCqnnjyD/GBRXQmE2U2sr3XEG3Hay3An0Dq/SyI=\n" }
    let(:initialization_vector) { "Vz\x7Fe\xBF\x98\x8F\xB3\xFC\xC1~\xD3'\xE4L\x06" }

    subject { described_class.execute(payload) }

    before do
      mock_config(:ENCRYPTION_KEY, test_key)
      allow_any_instance_of(OpenSSL::Cipher).to receive(:random_iv).and_return(initialization_vector)
    end

    it { is_expected.to eql(expected_result) }
  end
end
