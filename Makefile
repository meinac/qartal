DC=docker-compose -f docker-compose.yml $(1)
DC-RUN=$(call DC, run --rm qartal $(1))

usage:
	@echo "Available targets:"
	@echo "  * setup               - Setups everything for your convenience"
	@echo "  * build               - Builds image"
	@echo "  * console             - Starts the REPL"
	@echo "  * lint ARGS=args      - Runs rubocop {ARGS}"
	@echo "  * dev                 - Opens an ash session in the container"
	@echo "  * console             - Opens console"
	@echo "  * up                  - Runs the server"
	@echo "  * run                 - Runs the server and attaches only to it, allows you to use pry etc."
	@echo "  * down                - Removes all the containers and tears down the setup"
	@echo "  * stop                - Stops the server"
	@echo "  * test                - Runs rspec"

setup: build bundle db-prepare db-migrate

build:
	$(call DC, build)
bundle:
	$(call DC-RUN, bundle install)
db-prepare:
	$(call DC-RUN, ash -c "bin/rails db:prepare && bin/rails db:prepare RAILS_ENV=test")
db-migrate:
	$(call DC-RUN, bin/rails db:migrate)
console:
	$(call DC-RUN, rails c)
lint:
	$(call DC-RUN, rubocop ${ARGS})
dev:
	$(call DC-RUN, ash)
up:
	$(call DC, up)
run:
	$(call DC, run --service-ports qartal)
down:
	$(call DC, down --rmi all -v)
stop:
	$(call DC, stop)
test:
	$(call DC-RUN, bundle exec rspec ${ARGS})
