FROM ruby:3.0.2-alpine

COPY .build-deps ./
RUN cat .build-deps | xargs apk add

RUN mkdir /qartal
WORKDIR /qartal

ENV BUNDLE_PATH=/bundle \
    BUNDLE_BIN=/bundle/bin \
    GEM_HOME=/bundle
ENV PATH="${BUNDLE_BIN}:${PATH}"

COPY Gemfile Gemfile.lock ./
RUN bundle install --jobs 20 --retry 5
COPY . ./
